package com.cejv416.financecalculatorfx.app;

import com.cejv416.financecalculatorfx.calculator.FinanceCalculations;
import com.cejv416.financecalculatorfx.data.FinanceBean;
import com.cejv416.financecalculatorfx.presentation.FinanceCalculatorFX;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Here is the app class that does the initial set up and then transfers control
 * to the presentation class.
 *
 * @author Ken Fogel
 * @version 1.0
 */
public class FinanceCalculatorMain extends Application {

    private FinanceCalculations calc;
    private FinanceBean finance;
    private FinanceCalculatorFX gui;

    @Override
    public void init() {
        calc = new FinanceCalculations();
        finance = new FinanceBean();
        gui = new FinanceCalculatorFX(calc, finance);
    }

    @Override
    public void start(Stage primaryStage) {
        gui.start(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
