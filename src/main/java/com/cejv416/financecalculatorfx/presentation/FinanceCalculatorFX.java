package com.cejv416.financecalculatorfx.presentation;

import com.cejv416.financecalculatorfx.calculator.FinanceCalculations;
import com.cejv416.financecalculatorfx.data.FinanceBean;
import java.math.BigDecimal;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * This is the presentation class for the finance calculator
 *
 * @author Ken Fogel
 * @version 1.0
 */
public class FinanceCalculatorFX {

    private final FinanceCalculations calc;
    private final FinanceBean finance;

    private TextField amountValue;
    private TextField rateValue;
    private TextField termValue;
    private TextField resultValue;
    private Label amountLabel;
    private Label title;

    private int calculationType;

    /**
     * Non default constructor that receives the business logic and bean classes
     * This class then assumes the role of both view and controller
     *
     * @param calc
     * @param finance
     */
    public FinanceCalculatorFX(FinanceCalculations calc, FinanceBean finance) {
        this.calc = calc;
        this.finance = finance;
        calculationType = 0; // Loan
    }

    /**
     * This method creates a Label that is centered inside an HBox.
     *
     * @param text
     * @return
     */
    private HBox createTitle() {
        title = new Label("Loan Calculations"); // default

        // Style the label using CSS
        // Possible fonts can be found at http://www.webdesigndev.com/16-gorgeous-web-safe-fonts-to-use-with-css/
        title.setStyle("-fx-font-size:18pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");

        // To center the title and give it padding create an HBox, set the
        // padding and alignment, add the label and then add the HBox to the BorderPane.
        HBox hbox = new HBox();
        hbox.getChildren().add(title);
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(20, 20, 20, 20));

        return hbox;
    }

    /**
     * This method creates a BorderPane that has a title in the top and a
     * GridPane in the center. The GridPane contains a form.
     *
     * @return The BorderPane to add to the Scene
     */
    private BorderPane buildForm() {

        // Create the pane that will hold the controls
        BorderPane loanPane = new BorderPane();

        // Add a Title
        loanPane.setTop(createTitle());

        // Craete an empty GridPane
        GridPane loanGrid = new GridPane();

        // Column 0, Row 0
        amountLabel = new Label("Loan Amount");
        amountLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(amountLabel, 0, 0);

        // Column 1, Row 0
        amountValue = new TextField();
        amountValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        amountValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(amountValue, 1, 0);

        // Column 0, Row 1
        Label rateLabel = new Label("Interest Rate");
        rateLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(rateLabel, 0, 1);

        // Column 1, Row 1
        rateValue = new TextField();
        rateValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        rateValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(rateValue, 1, 1);

        // Column 0, Row 2
        Label termLabel = new Label("Term In Months");
        termLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(termLabel, 0, 2);

        // Column 1, Row 2
        termValue = new TextField();
        termValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        termValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(termValue, 1, 2);

        // Column 0, Row 3
        Label resultLabel = new Label("Result");
        resultLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(resultLabel, 0, 3);

        // Column 1, Row 3
        resultValue = new TextField();
        resultValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        resultValue.setAlignment(Pos.CENTER_RIGHT);
        resultValue.setEditable(false);
        loanGrid.add(resultValue, 1, 3);

        // Create a button and attach an event handler
        Button calculate = new Button("Calculate");
        calculate.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        calculate.setOnAction(this::calculateButtonHandler);

        // HBox that will span 2 columns so thatbutton can be centered across the GridPane
        HBox hbox = new HBox();
        hbox.getChildren().add(calculate);
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(20.0));
        loanGrid.add(hbox, 0, 4, 2, 1);

        // Set the column widths as a percentage
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(30.0);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(70.0);

        loanGrid.getColumnConstraints().addAll(col1, col2);

        // Add space around the outside of the GridPane
        loanGrid.setPadding(new Insets(20.0));
        // Add space between rows and columns of the GridPane
        loanGrid.setHgap(10.0);
        loanGrid.setVgap(10.0);

        // Load the GridPane into the pane
        loanPane.setCenter(loanGrid);

        return loanPane;
    }

    /**
     * This is the event handler when the button is being pressed. If there is a
     * NumberFormatException when any of the fields is converted to a BigDecimal
     * then an Alert box is displayed
     *
     * There is a better way to handle strings that cannot convert and you will
     * learn how in the Java Desktop course.
     *
     * @param e
     */
    private void calculateButtonHandler(ActionEvent e) {
        boolean doCalculation = true;
        try {
            finance.setInputValue(new BigDecimal(amountValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(amountValue.getText(), "Loan");
        }
        try {
            finance.setRate(new BigDecimal(rateValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(rateValue.getText(), "Rate");
        }
        try {
            finance.setTerm(new BigDecimal(termValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(termValue.getText(), "Term");
        }

        if (doCalculation == true) {
            switch (calculationType) {
                case 0 ->
                    calc.loanCalculation(finance);
                case 1 ->
                    calc.futureValueCalculation(finance);
                case 2 ->
                    calc.savingsGoalCalculation(finance);
            }
            resultValue.setText(finance.getResult().toString());
        }
    }

    /**
     * Display an Alert box if there is a NumberFormatException detected in the
     * calculateButtonHandler
     *
     * @param badValue
     * @param textField
     */
    private void numberFormatAlert(String badValue, String textField) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Number Format Error");
        alert.setHeaderText("The value \"" + badValue + "\" cannot be converted to a number for the " + textField);
        alert.setContentText("Number Format Error");

        alert.showAndWait();
    }

    /**
     * This method creates three radio buttons that are laid out horizontally in
     * an HBox.
     *
     * @return The HBox with the radio buttons
     */
    private HBox masterRadioButtons() {

        final ToggleGroup group = new ToggleGroup();

        RadioButton rbLoan = new RadioButton("Loan");
        rbLoan.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        rbLoan.setPadding(new Insets(10.0));
        rbLoan.setToggleGroup(group);
        rbLoan.setSelected(true);

        RadioButton rbSavings = new RadioButton("Savings");
        rbSavings.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        rbSavings.setPadding(new Insets(10.0));
        rbSavings.setToggleGroup(group);

        RadioButton rbGoal = new RadioButton("Goal");
        rbGoal.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        rbGoal.setPadding(new Insets(10.0));
        rbGoal.setToggleGroup(group);

        group.selectedToggleProperty().addListener(this::radioButtonListener);

        HBox hbox = new HBox();
        hbox.getChildren().addAll(rbLoan, rbSavings, rbGoal);
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(20.0));

        return hbox;
    }

    /**
     * This method is a changeListener that is called when a radio button is
     * selected from the ToggleGroup
     *
     * @param observableView
     * @param oldToggle
     * @param newToggle
     */
    private void radioButtonListener(ObservableValue<? extends Toggle> observableView, Toggle oldToggle, Toggle newToggle) {
        RadioButton rb = (RadioButton) newToggle.getToggleGroup().getSelectedToggle();

        switch (rb.getText()) {
            case "Loan" -> {
                calculationType = 0;
                title.setText("Loan Calculation");
                amountLabel.setText("Loan Amount");
            }
            case "Savings" -> {
                calculationType = 1;
                title.setText("Future Value of Savings Calculation");
                amountLabel.setText("Savings Amount");
            }
            case "Goal" -> {
                calculationType = 2;
                title.setText("Savings Goal Calculation");
                amountLabel.setText("Goal Amount");
            }
        }
        clearTextFields();
    }

    /**
     * This method clears all the text fields on the form
     */
    private void clearTextFields() {
        amountValue.setText("");
        rateValue.setText("");
        termValue.setText("");
        resultValue.setText("");
    }

    public void start(Stage primaryStage) {

        BorderPane root = new BorderPane();
        root.setTop(masterRadioButtons());
        root.setCenter(buildForm());

        Scene scene = new Scene(root, 600, 450);

        primaryStage.setTitle("Calculations");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
